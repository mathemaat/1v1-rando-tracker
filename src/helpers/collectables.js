import { COLLECTABLE_TYPES } from "../definitions/collectableTypes";
import { COLLECTABLES } from "../definitions/collectables";

export const getCollectableTypeByType = (type) => {
  return COLLECTABLE_TYPES.filter((collectable_type) => collectable_type.id === type)[0];
};

export const getItemsByType = (type) => {
  return COLLECTABLES.filter((item) => item.type === type);
};
