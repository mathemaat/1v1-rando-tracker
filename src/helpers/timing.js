export const formatDuration = (seconds) => {
  const steps = [60 * 60, 60, 1];

  let parts = [];
  for (let i = 0; i < steps.length; i++) {
    let step = steps[i];
    let number = Math.floor(seconds / step);
    seconds -= number * step;
    parts.push(number);
  }

  parts = parts.map((number) => String(number).padStart(2, "0"));

  return parts.join(":");
};

export const secondsSince = (from, to = new Date()) => {
  let startingTime = from.getTime();
  let currentTime = to.getTime();

  return Math.floor((currentTime - startingTime) / 1000);
};

export const timeSince = (from, to = new Date()) => {
  return formatDuration(secondsSince(from, to));
};

export const nextMillisecond = (date) => {
  const newDate = new Date(date);
  newDate.setMilliseconds(newDate.getMilliseconds() + 1);
  return newDate;
};
