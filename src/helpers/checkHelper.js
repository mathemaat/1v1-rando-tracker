import { AREAS } from "../definitions/areas";
import { CHECKS } from "../definitions/checks";
import { CHECK_TYPES } from "../definitions/checkTypes";
import { REWARDS, SONGS } from "../definitions/collectables";

export const getAreaById = (areaId) => {
  return AREAS.filter((area) => area.id === areaId)[0];
};

export const getAreasByIds = (areaIds) => {
  return areaIds.map((areaId) => getAreaById(areaId));
};

export const getAreasByType = (areaType) => {
  return AREAS.filter((area) => area.type === areaType);
};

export const getChecksInArea = (area) => {
  return CHECKS.filter((check) => check.areaId === area.id);
};

export const getCheckTypeById = (id) => {
  return CHECK_TYPES.filter((checkType) => checkType.id === id)[0];
};

export const getRewardById = (id) => {
  return REWARDS.filter((reward) => reward.id === id)[0];
};

export const getSongById = (id) => {
  return SONGS.filter((song) => song.id === id)[0];
};

export const getImageByCheck = (check) => {
  return (
    (check.rewardId && "rewards/" + getRewardById(check.rewardId).image) ||
    (check.songId && "songs/" + getSongById(check.songId).image) ||
    getCheckTypeById(check.checkTypeId).image
  );
};

export const checkIsDone = (check, pastChecks) => {
  return pastChecks.indexOf(check) !== -1;
};
