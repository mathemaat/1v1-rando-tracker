import { ACTION_TYPES } from "../definitions/actionTypes";
import { AREAS } from "../definitions/areas";

export const canAgeChange = (area) => {
  // only allow changing ages in Temple of Time (ToT)
  return area.id === "ToT";
};

export const canUseFW = (area) => {
  // only allow usage of Farores in dungeons (D) or side dungeons (SD)
  return ["D", "SD"].indexOf(area.type) !== -1;
};

export const getFaroresAt = (runnerState) => {
  return runnerState.fw[runnerState.age];
};

export const getActionPermissions = (runnerState) => {
  const canUseFarores = canUseFW(runnerState.area);

  return {
    canAgeChange: canAgeChange(runnerState.area),
    canUseFarores: canUseFarores,
    canDispelFarores: canUseFarores && getFaroresAt(runnerState) !== null,
  };
};

export const getActionTypeById = (id) => {
  return ACTION_TYPES.filter((actionType) => actionType.id === id)[0];
};

export const getPossibleSpawns = () => {
  return AREAS.filter((area) => area.isSpawn);
};
