export const CHECK_TYPE_BOSS = "BOSS";
export const CHECK_TYPE_CHEST = "CHEST";
export const CHECK_TYPE_FREESTANDING = "FREESTANDING";
export const CHECK_TYPE_MINIGAME = "MINIGAME";
export const CHECK_TYPE_NPC = "NPC";
export const CHECK_TYPE_REWARD = "REWARD";
export const CHECK_TYPE_SKULL = "SKULL";
export const CHECK_TYPE_SONG = "SONG";

export const CHECK_TYPES = [
  {
    id: CHECK_TYPE_BOSS,
    description: "Boss heart",
    pool: "items",
    image: "items/health/boss-heart.png",
    peekable: false,
  },
  {
    id: CHECK_TYPE_CHEST,
    description: "Regular check",
    pool: "items",
    image: "chest.png",
    peekable: true,
  },
  {
    id: CHECK_TYPE_FREESTANDING,
    description: "Freestanding item",
    pool: "items",
    image: "chest.png",
    peekable: true,
  },
  {
    id: CHECK_TYPE_MINIGAME,
    description: "Minigame",
    pool: "items",
    image: "rupee.png",
    peekable: false,
  },
  {
    id: CHECK_TYPE_NPC,
    description: "NPC",
    pool: "items",
    image: "chest.png",
    peekable: false,
  },
  {
    id: CHECK_TYPE_REWARD,
    description: "Dungeon reward",
    pool: "rewards",
    image: "reward.png",
    peekable: false,
  },
  {
    id: CHECK_TYPE_SKULL,
    description: "Skulltula token",
    pool: "skulls",
    image: "skulltula.png",
    peekable: false,
  },
  {
    id: CHECK_TYPE_SONG,
    description: "Song",
    pool: "songs",
    image: "song.png",
    peekable: false,
  },
];
