export const AREA_TYPES = [
  {
    id: "O",
    description: "Overworld",
  },
  {
    id: "T",
    description: "Town",
  },
  {
    id: "D",
    description: "Dungeon",
  },
  {
    id: "SD",
    description: "Side dungeon",
  },
];
