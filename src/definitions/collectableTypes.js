export const HEALTH = "HEALTH";
export const EQUIPMENT = "EQUIPMENT";
export const CLOTHING = "CLOTHING";
export const PROGRESSION = "PROGRESSION";
export const BOTTLE = "BOTTLE";
export const TRADE_QUEST = "TRADE_QUEST";
export const MEME = "MEME";
export const DUNGEON = "DUNGEON";
export const SONG = "SONG";
export const REWARD = "REWARD";

export const ITEM_TYPES = [
  {
    id: HEALTH,
    gridColsClass: "grid-cols-3",
    imageDir: "items/health/",
  },
  {
    id: EQUIPMENT,
    gridColsClass: "grid-cols-5",
    imageDir: "items/equipment/",
  },
  {
    id: CLOTHING,
    gridColsClass: "grid-cols-4",
    imageDir: "items/equipment/",
  },
  {
    id: PROGRESSION,
    gridColsClass: "grid-cols-4",
    imageDir: "items/progression/",
  },
  {
    id: BOTTLE,
    gridColsClass: "grid-cols-6",
    imageDir: "items/bottles/",
  },
  {
    id: TRADE_QUEST,
    gridColsClass: "grid-cols-4",
    imageDir: "items/trade-quest/",
  },
  {
    id: MEME,
    gridColsClass: "grid-cols-5",
    imageDir: "items/meme/",
  },
  {
    id: DUNGEON,
    gridColsClass: "grid-cols-2",
    imageDir: "items/dungeon/",
  },
];

export const SONG_TYPE = {
  id: SONG,
  gridColsClass: "grid-cols-6",
  imageDir: "songs/",
};

export const REWARD_TYPE = {
  id: REWARD,
  gridColsClass: "grid-cols-3",
  imageDir: "rewards/",
};

export const COLLECTABLE_TYPES = [].concat(
  ITEM_TYPES,
  [SONG_TYPE],
  [REWARD_TYPE],
);
