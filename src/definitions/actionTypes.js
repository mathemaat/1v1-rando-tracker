export const AGE_CHANGE = 'AGE_CHANGE';
export const AREA_SET = 'AREA_SET';
export const CHECK = 'CHECK';
export const FARORES_DISPEL = 'FARORES_DISPEL';
export const FARORES_SET = 'FARORES_SET';
export const FARORES_WARP = 'FARORES_WARP';
export const HIKE = 'HIKE';
export const PLAY_WARP_SONG = 'PLAY_WARP_SONG';
export const SPAWN = 'SPAWN';

export const ACTION_TYPES = [
  {
    id: AGE_CHANGE,
    description: "Go {age}",
    image: "master-sword.png",
  },
  {
    id: AREA_SET,
    description: "Set area to {area}",
    image: "boots.png",
  },
  {
    id: CHECK,
    description: "{area} {check}",
    image: "unknown.png",
  },
  {
    id: FARORES_DISPEL,
    description: "Dispel (was {area})",
    image: "farores-dispel.png",
  },
  {
    id: FARORES_SET,
    description: "Set in {area}",
    image: "farores.png",
  },
  {
    id: FARORES_WARP,
    description: "Warp to {area}",
    image: "farores.png",
  },
  {
    id: HIKE,
    description: "Go to {area}",
    image: "boots.png",
  },
  {
    id: PLAY_WARP_SONG,
    description: "Play {song}",
    image: "ocarina.png",
  },
  {
    id: SPAWN,
    description: "Spawn in {area} as {age}",
    image: "boots.png",
  },
];
