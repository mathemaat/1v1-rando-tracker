import {
  HEALTH,
  EQUIPMENT,
  CLOTHING,
  PROGRESSION,
  BOTTLE,
  TRADE_QUEST,
  MEME,
  DUNGEON,
  SONG,
  REWARD,
} from "./collectableTypes";

export const ITEMS = [
  {
    id: "boss-heart",
    type: HEALTH,
    name: "Boss hart container",
  },
  {
    id: "heart-piece",
    type: HEALTH,
    name: "Heart piece",
  },
  {
    id: "magic",
    type: HEALTH,
    name: "Progressive magic",
  },
  {
    id: "kokiri-sword",
    type: EQUIPMENT,
    name: "Kokiri sword",
  },
  {
    id: "biggoron-sword",
    type: EQUIPMENT,
    name: "Biggoron sword",
  },
  {
    id: "deku-shield",
    type: EQUIPMENT,
    name: "Deku shield",
  },
  {
    id: "hyrule-shield",
    type: EQUIPMENT,
    name: "Hyrule shield",
  },
  {
    id: "mirror-shield",
    type: EQUIPMENT,
    name: "Mirror shield",
  },
  {
    id: "goron-tunic",
    type: CLOTHING,
    name: "Goron tunic",
  },
  {
    id: "zora-tunic",
    type: CLOTHING,
    name: "Zora tunic",
  },
  {
    id: "iron-boots",
    type: CLOTHING,
    name: "Iron boots",
  },
  {
    id: "hover-boots",
    type: CLOTHING,
    name: "Hover boots",
  },
  {
    id: "deku-stick",
    type: PROGRESSION,
    name: "Deku stick upgrade",
  },
  {
    id: "deku-nut",
    type: PROGRESSION,
    name: "Deku nut upgrade",
  },
  {
    id: "slingshot",
    type: PROGRESSION,
    name: "Progressive slingshot",
  },
  {
    id: "boomerang",
    type: PROGRESSION,
    name: "Boomerang",
  },
  {
    id: "strength",
    type: PROGRESSION,
    name: "Progressive strength",
  },
  {
    id: "bow",
    type: PROGRESSION,
    name: "Progressive bow",
  },
  {
    id: "bomb",
    type: PROGRESSION,
    name: "Progressive bomb bag",
  },
  {
    id: "bombchu",
    type: PROGRESSION,
    name: "Bombchus",
  },
  {
    id: "hookshot",
    type: PROGRESSION,
    name: "Progressive hookshot",
  },
  {
    id: "hammer",
    type: PROGRESSION,
    name: "Megaton hammer",
  },
  {
    id: "lens",
    type: PROGRESSION,
    name: "Magic lens",
  },
  {
    id: "zora-scale",
    type: PROGRESSION,
    name: "Progressive scale",
  },
  {
    id: "dins-fire",
    type: PROGRESSION,
    name: "Din's fire",
  },
  {
    id: "farores-wind",
    type: PROGRESSION,
    name: "Farore's wind",
  },
  {
    id: "fire-arrows",
    type: PROGRESSION,
    name: "Fire arrows",
  },
  {
    id: "light-arrows",
    type: PROGRESSION,
    name: "Light arrows",
  },
  {
    id: "rutos-letter",
    type: BOTTLE,
    name: "Ruto's letter",
  },
  {
    id: "empty-bottle",
    type: BOTTLE,
    name: "Empty bottle",
  },
  {
    id: "green-potion",
    type: BOTTLE,
    name: "Green potion",
  },
  {
    id: "red-potion",
    type: BOTTLE,
    name: "Red potion",
  },
  {
    id: "blue-potion",
    type: BOTTLE,
    name: "Blue potion",
  },
  {
    id: "milk",
    type: BOTTLE,
    name: "Milk",
  },
  {
    id: "fairy",
    type: BOTTLE,
    name: "Fairy",
  },
  {
    id: "blue-fire",
    type: BOTTLE,
    name: "Blue fire",
  },
  {
    id: "bugs",
    type: BOTTLE,
    name: "Bugs",
  },
  {
    id: "fish",
    type: BOTTLE,
    name: "Fish",
  },
  {
    id: "big-poe",
    type: BOTTLE,
    name: "Big poe",
  },
  {
    id: "poe",
    type: BOTTLE,
    name: "Poe",
  },
  {
    id: "prescription",
    type: TRADE_QUEST,
    name: "Prescription",
  },
  {
    id: "frog",
    type: TRADE_QUEST,
    name: "Frog",
  },
  {
    id: "eyedrops",
    type: TRADE_QUEST,
    name: "Eyedrops",
  },
  {
    id: "claim-check",
    type: TRADE_QUEST,
    name: "Claim check",
  },
  {
    id: "greg",
    type: MEME,
    name: "Greg",
  },
  {
    id: "stone-of-agony",
    type: MEME,
    name: "Stone of agony",
  },
  {
    id: "ice-arrows",
    type: MEME,
    name: "Ice arrows",
  },
  {
    id: "nayrus-love",
    type: MEME,
    name: "Nayru's love",
  },
  {
    id: "double-defense",
    type: MEME,
    name: "Double defense",
  },
  {
    id: "boss-key",
    type: DUNGEON,
    name: "Boss key",
  },
  {
    id: "small-key",
    type: DUNGEON,
    name: "Small key",
  },
];

export const SONG_ZL = "ZL";
export const SONG_EPONAS = "EPONA'S";
export const SONG_SARIAS = "SARIA'S";
export const SONG_SUNS = "SUN'S";
export const SONG_OF_TIME = "SOT";
export const SONG_OF_STORMS = "SOS";
export const SONG_MINUET = "MINUET";
export const SONG_BOLERO = "BOLERO";
export const SONG_SERENADE = "SERENADE";
export const SONG_REQUIEM = "REQUIEM";
export const SONG_SHADOW = "SHADOW";
export const SONG_PRELUDE = "PRELUDE";

export const CHILD_SONGS = [
  {
    id: SONG_ZL,
    type: SONG,
    name: "Zelda's lullaby",
    image: "zeldas-lullaby.png",
  },
  {
    id: SONG_EPONAS,
    type: SONG,
    name: "Epona's song",
    image: "eponas-song.png",
  },
  {
    id: SONG_SARIAS,
    type: SONG,
    name: "Saria's song",
    image: "sarias-song.png",
  },
  {
    id: SONG_SUNS,
    type: SONG,
    name: "Sun's song",
    image: "suns-song.png",
  },
  {
    id: SONG_OF_TIME,
    type: SONG,
    name: "Song of time",
    image: "song-of-time.png",
  },
  {
    id: SONG_OF_STORMS,
    type: SONG,
    name: "Song of storms",
    image: "song-of-storms.png",
  },
];

export const WARP_SONGS = [
  {
    id: SONG_MINUET,
    type: SONG,
    name: "Minuet",
    areaId: "SFM",
    image: "minuet.png",
  },
  {
    id: SONG_BOLERO,
    type: SONG,
    name: "Bolero",
    areaId: "DMC",
    image: "bolero.png",
  },
  {
    id: SONG_SERENADE,
    type: SONG,
    name: "Serenade",
    areaId: "LH",
    image: "serenade.png",
  },
  {
    id: SONG_REQUIEM,
    type: SONG,
    name: "Requiem",
    areaId: "Col",
    image: "requiem.png",
  },
  {
    id: SONG_SHADOW,
    type: SONG,
    name: "Nocturne",
    areaId: "GY",
    image: "nocturne.png",
  },
  {
    id: SONG_PRELUDE,
    type: SONG,
    name: "Prelude",
    areaId: "ToT",
    image: "prelude.png",
  },
];

export const SONGS = [].concat(CHILD_SONGS, WARP_SONGS);

export const REWARD_DEKU = "KOKIRI_EMERALD";
export const REWARD_DC = "GORON_RUBY";
export const REWARD_JABU = "ZORA_SAPPHIRE";
export const REWARD_FOREST = "FOREST_MEDALLION";
export const REWARD_FIRE = "FIRE_MEDALLION";
export const REWARD_WATER = "WATER_MEDALLION";
export const REWARD_SPIRIT = "SPIRIT_MEDALLION";
export const REWARD_SHADOW = "SHADOW_MEDALLION";
export const REWARD_TEMPLE = "LIGHT_MEDALLION";

export const REWARDS = [
  {
    id: REWARD_DEKU,
    type: REWARD,
    name: "Kokiri emerald",
    image: "kokiri-emerald.png",
  },
  {
    id: REWARD_DC,
    type: REWARD,
    name: "Goron ruby",
    image: "goron-ruby.png",
  },
  {
    id: REWARD_JABU,
    type: REWARD,
    name: "Zora sapphire",
    image: "zora-sapphire.png",
  },
  {
    id: REWARD_FOREST,
    type: REWARD,
    name: "Forest medallion",
    image: "forest-medallion.png",
  },
  {
    id: REWARD_FIRE,
    type: REWARD,
    name: "Fire medallion",
    image: "fire-medallion.png",
  },
  {
    id: REWARD_WATER,
    type: REWARD,
    name: "Water medallion",
    image: "water-medallion.png",
  },
  {
    id: REWARD_SPIRIT,
    type: REWARD,
    name: "Spirit medallion",
    image: "spirit-medallion.png",
  },
  {
    id: REWARD_SHADOW,
    type: REWARD,
    name: "Shadow medallion",
    image: "shadow-medallion.png",
  },
  {
    id: REWARD_TEMPLE,
    type: REWARD,
    name: "Light medallion",
    image: "light-medallion.png",
  },
];

export const COLLECTABLES = [].concat(ITEMS, SONGS, REWARDS);
