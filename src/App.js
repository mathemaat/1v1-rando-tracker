import { getPossibleSpawns } from "./helpers/actionHelper";
import GameTime from "./components/GameTime";
import Runner from "./components/Runner";
import "./App.css";
import { useState } from "react";

const gameStart = new Date(Date.now());

function App() {
  // temporary values for starting age and spawns
  const startingAge = Math.random() < 0.5 ? "child" : "adult";

  let possibleSpawns = getPossibleSpawns();

  const spawns = {
    child: possibleSpawns.random(),
    adult: possibleSpawns.random(),
  };

  const [itemLayout, setItemLayout] = useState({});

  const collectableHandler = (check, collectable) => {
    let key = check.areaId + " " + check.name;

    setItemLayout((previous) => {
      let newState = { ...previous };
      newState[key] = collectable;
      return newState;
    });
  };

  return (
    <div className="p-8 text-sm">
      <h1>1v1 Rando tracker</h1>
      <GameTime gameStart={gameStart} />
      <div className="flex">
        <Runner
          name="Link"
          gameStart={gameStart}
          startingAge={startingAge}
          spawns={spawns}
          itemLayout={itemLayout}
          onRegisterCollectable={collectableHandler}
        />
        <Runner
          name="Lonk"
          gameStart={gameStart}
          startingAge={startingAge}
          spawns={spawns}
          itemLayout={itemLayout}
          onRegisterCollectable={collectableHandler}
        />
      </div>
    </div>
  );
}

export default App;
