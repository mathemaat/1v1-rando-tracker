const ActionButton = (props) => {
  let classes = [];
  classes.push(props.type === "icon" ? "p-1" : "px-1");
  classes = classes.concat(
    props.enabled
      ? ["cursor-pointer", "hover:border-black"]
      : ["cursor-not-allowed"]
  );
  let classNames = classes.join(" ");

  return (
    <div
      className={`rounded-lg border border-gray-500 bg-gray-200 ${classNames}`}
      title={props.tooltip}
      onClick={props.enabled ? props.onClickHandler : null}
    >
      {props.children}
    </div>
  );
};

export default ActionButton;
