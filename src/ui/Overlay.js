import Card from "./Card";

const Overlay = (props) => {
  return (
    <Card
      className="absolute mx-auto inset-x-0 top-20 text-center"
      bgColor="bg-black"
      borderColor="border-gray-600"
    >
      <h1 className="pb-2 mb-2 border-b-4 border-gray-600 text-gray-400 text-xl font-bold">{props.title}</h1>
      <div>{props.children}</div>
    </Card>
  );
};

export default Overlay;
