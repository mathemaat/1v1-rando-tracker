const Card = (props) => {
  return (
    <div
      className={`w-96 p-4 rounded-xl border-4 ${props.className} ${props.bgColor} ${props.borderColor}`}
    >
      {props.children}
    </div>
  );
};

export default Card;
