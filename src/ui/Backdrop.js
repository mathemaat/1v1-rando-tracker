const Backdrop = (props) => {
  return (
    <div className="absolute inset-0 bg-black/40" onClick={props.onClose}>
      {props.children}
    </div>
  );
};

export default Backdrop;
