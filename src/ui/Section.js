const Section = (props) => {
  return (
    <div>
      <h3>{props.label}:</h3>
      <div className="mb-2">{props.children}</div>
    </div>
  );
};

export default Section;
