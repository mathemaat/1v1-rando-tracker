import { getItemsByType } from "../helpers/collectables";

const CollectableGrid = (props) => {
  const collectables = getItemsByType(props.collectableType.id);

  return (
    <div>
      <div
        className={`inline-grid ${props.collectableType.gridColsClass} place-items-center`}
      >
        {collectables.map((collectable) => {
          const imageSrc =
            "../images/" +
            props.collectableType.imageDir +
            (collectable.image || collectable.id + ".png");

          return (
            <img
              key={collectable.id}
              className="cursor-pointer p-1 border-2 border-black hover:border-amber-300"
              src={imageSrc}
              title={collectable.name}
              alt={collectable.id}
              onClick={(event) => props.onSelectCollectable(collectable)}
            />
          );
        })}
      </div>
    </div>
  );
};

export default CollectableGrid;
