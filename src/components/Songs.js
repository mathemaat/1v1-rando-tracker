import { WARP_SONGS } from "../definitions/collectables";
import ActionButton from "../ui/ActionButton";

const Songs = (props) => {
  return (
    <div className="flex space-x-0.5">
      {WARP_SONGS.map((song) => {
        const imageSrc = "../images/songs/" + song.image;

        return (
          <ActionButton
            key={song.id}
            type="icon"
            tooltip={song.name}
            enabled={true}
            onClickHandler={(event) => props.onPlaySong(song)}
          >
            <img className="h-4 w-4" src={imageSrc} alt={song.id} />
          </ActionButton>
        );
      })}
    </div>
  );
};

export default Songs;
