import { getActionPermissions, getFaroresAt } from "../helpers/actionHelper";
import ActionButton from "../ui/ActionButton";
import Songs from "./Songs";

const GlobalActions = (props) => {
  const actionPermissions = getActionPermissions(props.runnerState);

  const faroresAt = getFaroresAt(props.runnerState);
  const faroresTooltip =
    faroresAt !== null ? "Go to " + faroresAt.name : "Set Farore's";

  return (
    <div className="flex">
      <Songs onPlaySong={props.onPlaySong} />
      <div className="flex space-x-0.5 ml-2">
        <ActionButton
          tooltip="Age change"
          type="icon"
          enabled={actionPermissions.canAgeChange}
          onClickHandler={props.onAgeChange}
        >
          <img className="w-4 h-4" src="../images/master-sword.png" alt="ped" />
        </ActionButton>
        <ActionButton
          tooltip={faroresTooltip}
          type="icon"
          enabled={actionPermissions.canUseFarores}
          onClickHandler={(event) => props.onUseFarores(props.runnerState.area)}
        >
          <img className="w-4 h-4" src="../images/farores.png" alt="fw" />
        </ActionButton>
        <ActionButton
          tooltip="Dispel Farore's"
          type="icon"
          enabled={actionPermissions.canDispelFarores}
          onClickHandler={props.onDispelFarores}
        >
          <img className="w-4 h-4" src="../images/farores-dispel.png" alt="fw" />
        </ActionButton>
      </div>
    </div>
  );
};

export default GlobalActions;
