import { useState, useReducer, useEffect } from "react";
import { getFaroresAt } from "../helpers/actionHelper";
import { getAreaById } from "../helpers/checkHelper";
import Card from "../ui/Card";
import Section from "../ui/Section";
import GlobalActions from "./GlobalActions";
import AvailableChecks from "./AvailableChecks";
import AreaSelect from "./AreaSelect";
import ActionHistory from "./ActionHistory";
import HeadingTo from "./HeadingTo.js";
import { nextMillisecond } from "../helpers/timing";

import {
  AGE_CHANGE,
  AREA_SET,
  CHECK,
  FARORES_DISPEL,
  FARORES_SET,
  FARORES_WARP,
  HIKE,
  PLAY_WARP_SONG,
  SPAWN,
} from "../definitions/actionTypes";

import {
  LINKS_POCKET,
  SONG_FROM_IMPA,
} from "../definitions/checks";

const runnerReducer = (state, action) => {
  let newState = { ...state, fw: { ...state.fw } };

  switch (action.type) {
    case AGE_CHANGE:
      newState.age = action.age;
      break;
    case FARORES_SET:
      newState.fw[state.age] = action.area;
      break;
    case FARORES_DISPEL:
      newState.fw[state.age] = null;
      break;
    case FARORES_WARP:
      newState.area = state.fw[state.age];
      newState.fw[state.age] = null;
      break;
    case AREA_SET:
    case HIKE:
      newState.area = action.area;
      break;
    case PLAY_WARP_SONG:
      newState.area = getAreaById(action.song.areaId);
      break;
    default:
      break;
  }

  return newState;
};

const Runner = (props) => {
  const startingArea = props.spawns[props.startingAge];
  const startingAge = props.startingAge;

  const [runnerState, runnerDispatch] = useReducer(runnerReducer, {
    area: startingArea,
    age: startingAge,
    fw: {
      child: null,
      adult: null,
    },
  });

  const prepareStartingActions = (age, spawningArea) => {
    let actions = [];

    let timestamp = props.gameStart;
    // checks.push({
    //   time: props.gameStart,
    //   type: "start",
    // });

    // timestamp = nextMillisecond(timestamp);
    actions.push({
      time: timestamp,
      type: CHECK,
      check: LINKS_POCKET,
    });

    timestamp = nextMillisecond(timestamp);
    actions.push({
      time: timestamp,
      type: CHECK,
      check: SONG_FROM_IMPA,
    });

    timestamp = nextMillisecond(timestamp);
    actions.push({
      time: timestamp,
      type: SPAWN,
      age: age,
      area: spawningArea,
    });

    return actions;
  };

  const initialActionHistory = prepareStartingActions(
    startingAge,
    startingArea,
    null
  );

  const [actionHistory, setActionHistory] = useState(initialActionHistory);

  const getPastChecks = () => {
    return actionHistory
      .filter((action) => {
        return action.type === CHECK;
      })
      .map((action) => {
        return action.check;
      });
  };

  const incrementActionHistory = (checkHistory, actionData) => {
    return [
      ...checkHistory,
      {
        time: new Date(),
        ...actionData,
      },
    ];
  };

  const areaChangeHandler = (area) => {
    setActionHistory((previous) => {
      return incrementActionHistory(previous, {
        type: AREA_SET,
        area: area,
      });
    });
  };

  const ageChangeHandler = () => {
    setActionHistory((previous) => {
      return incrementActionHistory(previous, {
        type: AGE_CHANGE,
        age: runnerState.age === "child" ? "adult" : "child",
      });
    });
  };

  const useFaroresHandler = (area) => {
    let faroresAt = getFaroresAt(runnerState);
    let action = null;
    if (faroresAt === null) {
      action = {
        type: FARORES_SET,
        area: area,
      };
    } else {
      action = {
        type: FARORES_WARP,
        area: faroresAt,
      };
    }

    setActionHistory((previous) => {
      return incrementActionHistory(previous, action);
    });
  };

  const dispelFaroresHandler = () => {
    setActionHistory((previous) => {
      return incrementActionHistory(previous, {
        type: FARORES_DISPEL,
        area: getFaroresAt(runnerState),
      });
    });
  };

  const hikeHandler = (area) => {
    setActionHistory((previous) => {
      return incrementActionHistory(previous, {
        type: HIKE,
        area: area,
      });
    });
  };

  const songHandler = (song) => {
    setActionHistory((previous) => {
      return incrementActionHistory(previous, {
        type: PLAY_WARP_SONG,
        song: song,
      });
    });
  };

  const checkHandler = (check) => {
    setActionHistory((previous) => {
      return incrementActionHistory(previous, {
        type: CHECK,
        check: check,
      });
    });
  };

  useEffect(() => {
    runnerDispatch(actionHistory[actionHistory.length - 1]);
  }, [actionHistory]);

  return (
    <Card
      className="mr-4"
      bgColor="bg-stone-300 "
      borderColor="border-stone-600"
    >
      <h2 className="mb-2 text-lg font-bold underline">{props.name}</h2>
      <Section label="Currently in">
        <AreaSelect selected={runnerState.area} onAreaSelect={areaChangeHandler} />
        <span className="pl-1">as {runnerState.age}</span>
      </Section>
      <Section label="Heading to">
        <HeadingTo area={runnerState.area} onHike={hikeHandler} />
      </Section>
      <Section label="Global actions">
        <GlobalActions
          runnerState={runnerState}
          onPlaySong={songHandler}
          onAgeChange={ageChangeHandler}
          onUseFarores={useFaroresHandler}
          onDispelFarores={dispelFaroresHandler}
        />
      </Section>
      <Section label="Available checks">
        <AvailableChecks
          area={runnerState.area}
          pastChecks={getPastChecks()}
          onCheckDo={checkHandler}
        />
      </Section>
      <Section label="Action history">
        <ActionHistory
          actions={actionHistory}
          gameStart={props.gameStart}
          itemLayout={props.itemLayout}
          onRegisterCollectable={props.onRegisterCollectable}
        />
      </Section>
    </Card>
  );
};

export default Runner;
