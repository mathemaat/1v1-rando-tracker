import CollectableGrid from "./CollectableGrid";
import Overlay from "../ui/Overlay";

const CollectableOverlay = (props) => {
  return (
    <Overlay title={props.title}>
      {props.collectableTypes.map((collectableType) => (
        <CollectableGrid
          key={collectableType.id}
          collectableType={collectableType}
          onSelectCollectable={props.onSelectCollectable}
        />
      ))}
    </Overlay>
  );
};

export default CollectableOverlay;
