import { useEffect, useState } from "react";
import { timeSince } from "../helpers/timing";

const GameTime = (props) => {
  const initialGameTime = timeSince(props.gameStart);
  const [gameTime, setGameTime] = useState(initialGameTime);

  const refreshGameTime = () => {
    setGameTime(timeSince(props.gameStart));
  };

  useEffect(() => {
    const interval = setInterval(refreshGameTime, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div>Game time: {gameTime}</div>
  );
};

export default GameTime;
