import ActionHistoryItem from "./ActionHistoryItem";

const ActionHistory = (props) => {
  return (
    <ul>
      {props.actions.map((action) => {
        return (
          <ActionHistoryItem
            key={action.time.getTime()}
            action={action}
            gameStart={props.gameStart}
            itemLayout={props.itemLayout}
            onRegisterCollectable={props.onRegisterCollectable}
          />
        );
      })}
    </ul>
  );
};

export default ActionHistory;
