import { getAreasByIds } from "../helpers/checkHelper";
import ActionButton from "../ui/ActionButton";

const HeadingTo = (props) => {
  const adjacentAreas = getAreasByIds(props.area.adjacent);

  return (
    <div className="flex space-x-2">
      {adjacentAreas.map((area) => {
        return (
          <ActionButton
            key={area.id}
            type="text"
            tooltip={area.name}
            enabled={true}
            onClickHandler={(event) => props.onHike(area)}
          >
            {area.id}
          </ActionButton>
        );
      })}
    </div>
  );
};

export default HeadingTo;
