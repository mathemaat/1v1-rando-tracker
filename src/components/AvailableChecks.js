import {
  checkIsDone,
  getChecksInArea,
  getImageByCheck,
} from "../helpers/checkHelper";

const AvailableChecks = (props) => {
  const checks = getChecksInArea(props.area);

  const checkClickHandler = (event, check) => {
    props.onCheckDo(check);
  };

  return (
    <ul>
      {checks.map((check) => {
        const imageSrc = "../images/" + getImageByCheck(check);

        const isDone = checkIsDone(check, props.pastChecks);
        const doneClass = isDone ? "hover:border-transparent" : "hover:border-black cursor-pointer";

        return (
          <li
            key={check.name}
            className={`px-1 border border-transparent ${doneClass}`}
            onClick={(event) => !isDone ? checkClickHandler(event, check) : {}}
          >
            <div className="flex w-full items-center">
              <img src={imageSrc} className="h-4 w-4 mr-1" alt={check.name} />
              <span>{check.name} {isDone ? '✔' : ''}</span>
            </div>
          </li>
        );
      })}
    </ul>
  );
};

export default AvailableChecks;
