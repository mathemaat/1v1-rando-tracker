import { AREA_TYPES } from "../definitions/areaTypes";
import { getAreaById, getAreasByType } from "../helpers/checkHelper";

const AreaSelect = (props) => {
  const selectChangeHandler = (event) => {
    let area = getAreaById(event.target.value)
    props.onAreaSelect(area);
  };

  return (
    <select className="border border-gray-500 bg-gray-200" value={props.selected.id} onChange={selectChangeHandler}>
      {AREA_TYPES.map((areaType) => {
        let areas = getAreasByType(areaType.id);
        return (
          <optgroup key={areaType.id} label={areaType.description}>
            {areas.map((area) => {
              return (
                <option
                  key={area.id}
                  value={area.id}
                  className="text-sm"
                >
                  {area.name}
                </option>
              );
            })}
          </optgroup>
        );
      })}
    </select>
  );
};

export default AreaSelect;
