import { useState } from "react";
import ReactDOM from "react-dom";
import { CHECK } from "../definitions/actionTypes";
import {
  ITEM_TYPES,
  SONG_TYPE,
  REWARD_TYPE,
} from "../definitions/collectableTypes";
import { getActionTypeById } from "../helpers/actionHelper";
import { getCheckTypeById, getImageByCheck } from "../helpers/checkHelper";
import { getCollectableTypeByType } from "../helpers/collectables";
import { timeSince } from "../helpers/timing";
import Backdrop from "../ui/Backdrop";
import CollectableOverlay from "./CollectableOverlay";

const ActionHistoryItem = (props) => {
  const ingameTime = timeSince(props.gameStart, props.action.time);

  const actionType = getActionTypeById(props.action.type);

  let actionDescription = "";
  let actionImageSrc = "../images/";
  let collectableImageSrc = "../images/unknown.png";
  let collectableImageAlt = "unknown";
  let collectableTitle = "";
  let collectableTypes = [];

  if (actionType.id === CHECK) {
    actionDescription = actionType.description.formatUnicorn({
      area: props.action.check.areaId,
      check: props.action.check.name,
    });
    actionImageSrc += getImageByCheck(props.action.check);

    let checkType = getCheckTypeById(props.action.check.checkTypeId);
    switch (checkType.pool) {
      case "items":
        collectableTitle = "Items";
        collectableTypes = ITEM_TYPES;
        break;
      case "songs":
        collectableTitle = "Songs";
        collectableTypes = [SONG_TYPE];
        break;
      case "rewards":
        collectableTitle = "Rewards";
        collectableTypes = [REWARD_TYPE];
        break;
      default:
        collectableTitle = "";
        collectableTypes = [];
        break;
    }

    let key = props.action.check.areaId + " " + props.action.check.name;
    let collectable = props.itemLayout[key] || null;
    if (collectable) {
      let collectableType = getCollectableTypeByType(collectable.type);
      collectableImageSrc =
        "../images/" +
        collectableType.imageDir +
        (collectable.image || collectable.id + ".png");
      collectableImageAlt = collectable.name;
    }
  } else {
    actionDescription = actionType.description.formatUnicorn({
      age: props.action.age || "",
      area: (props.action.area && props.action.area.id) || "",
      song: (props.action.song && props.action.song.name) || "",
    });
    actionImageSrc += actionType.image;
  }

  const hasItemGrid = collectableTypes.length >= 1;

  const [showItemGrid, setShowItemGrid] = useState(false);

  const openGrid = () => {
    setShowItemGrid(true);
  };

  const closeGrid = () => {
    setShowItemGrid(false);
  };

  const selectCollectableHandler = (item) => {
    closeGrid();
    props.onRegisterCollectable(props.action.check, item);
  };

  return (
    <li className="px-1 border border-transparent">
      <div className="flex justify-between space-x-2">
        <div className="flex">
          <div>{ingameTime}</div>
        </div>
        <div className="flex w-full space-x-1">
          <img
            src={actionImageSrc}
            className="h-4 w-4 my-0.5"
            alt={actionDescription}
          />
          <span>{actionDescription}</span>
        </div>
        {hasItemGrid && (
          <div className="flex w-6">
            <img
              src={collectableImageSrc}
              className="h-4 w-4 my-0.5 cursor-pointer"
              alt={collectableImageAlt}
              onClick={openGrid}
            />
          </div>
        )}
      </div>
      {hasItemGrid && showItemGrid && (
        <>
          {ReactDOM.createPortal(
            <Backdrop onClose={closeGrid} />,
            document.getElementById("backdrop")
          )}
          {ReactDOM.createPortal(
            <CollectableOverlay
              title={collectableTitle}
              collectableTypes={collectableTypes}
              onSelectCollectable={selectCollectableHandler}
            />,
            document.getElementById("overlay")
          )}
        </>
      )}
    </li>
  );
};

export default ActionHistoryItem;
